export interface User {
    id: string;
    username: string;
    email: string;
    credentials: [{
        type: string;
        value: string;
    }]
}