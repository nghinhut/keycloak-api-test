export interface Uma2Configuration {
    token_endpoint: string
    token_introspection_endpoint: string
    resource_registration_endpoint: string
    permission_endpoint: string
    policy_endpoint: string
}