var $evaluation = $evaluation;
// @ts-ignore
var print

var context = $evaluation.getContext();
var contextAttributes = context.getAttributes();
var realm = $evaluation.getRealm();
var identity = context.getIdentity();
var attributes = identity.getAttributes();
var userId = identity.getId();
var resourcePermission = $evaluation.getPermission();
var resource = resourcePermission.getResource();
var resourceScopes = resource.getScopes();
var resourceServer = resourcePermission.getResourceServer();
var resourceUserGroups = realm.getUserGroups(identity.getId());
// var resourcesId = resources.getId();

if (realm.isUserInGroup(userId, 'pi.groups.ioh.products.vdatlab.com')){
    print('yes');
} else {
    print('no');
}
print('identity attributes: ', identity.getAttributes());
print('context.getAttributes: ', context.getAttributes());
print(resourcePermission.getClaims());
print('resourceId: ' + resource.getId());
print('resourceName: ' + resource.getName());
print('resourceOwner: ' + resource.getOwner());
print(resource.getDisplayName());
print('resource.getAttributes: ', resource.getAttributes());
for (var i = 0 ; i < resourceScopes.length; i++) {
    print(resourceScopes[i].getName())
}

print('resource.isFetched("attributes")', resource.isFetched("attributes"));
print('resourceServerId: ', resourceServer.getId());
print('resourceServer.isAllowRemoteResourceManagement: ', resourceServer.isAllowRemoteResourceManagement());
print(resourceUserGroups);
print(realm.getUserAttributes(userId));

$evaluation.grant();
