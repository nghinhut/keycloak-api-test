import * as _ from 'lodash';
import {OpenidConfiguation} from "./interfaces/openid-configuation";
import {Token} from "./interfaces/token";
import {User} from "./interfaces/user";
import {Uma2Configuration} from "./interfaces/uma2-configuration";

const request = require('request');
const rp = require('request-promise');

const KEYCLOAK_URL = 'http://keycloak:8080';
let OPENID_CONFIG: OpenidConfiguation;
let UMA2_CONFIG: Uma2Configuration;
const DEFAULT_USER_PASSWORD = '123';
let token: Token;
let realmManagementClient;
let myAppClient;
let productAGroup;
// @ts-ignore
let users: {
    allowCreateProject: User
} = {};

describe('Init tests', () => {
    it('Keycloak should be running', done => {
        request({
            url: KEYCLOAK_URL + '/auth/realms/master',
            json: true
        }, (error, response, body) => {
            if (error) {
                console.log(error)
            } else {
                // console.log(response)
                // console.log(body)
            }
            expect(error).toBeNull();
            expect(body.realm).toEqual('master');
            expect(body.public_key).toBeDefined();
            done();
        })
    });
    it('get oidc configuration', done => {
        request({
            url: KEYCLOAK_URL + '/auth/realms/master/.well-known/openid-configuration',
            json: true
        }, (error, response, body) => {
            if (error) {
                console.log(error)
            } else {
                // console.log(response)
                // console.log(body)
            }
            expect(error).toBeNull();
            expect(body).toBeDefined();
            OPENID_CONFIG = body;
            done();
        })
    });
    it('get access token', done => {
        const options = {
            method: 'POST',
            url: OPENID_CONFIG.token_endpoint,
            form: {
                grant_type: 'password',
                client_id: 'admin-cli',
                username: process.env.KEYCLOAK_USER,
                password: process.env.KEYCLOAK_PASSWORD,
            },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(error).toBeNull();
            expect(body).toBeDefined();
            token = body;
            done();
        });
    });
    it('Update Realm Master configuration', done => {
        const options = {
            method: 'PUT',
            url: KEYCLOAK_URL + '/auth/admin/realms/master',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {
                accessTokenLifespan: 3600,
            },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(error).toBeNull();
            expect(response.statusCode).toEqual(204);
            console.log(body);
            done();
        });
    });
    it('get access token 2', done => {
        const options = {
            method: 'POST',
            url: OPENID_CONFIG.token_endpoint,
            form: {
                grant_type: 'password',
                client_id: 'admin-cli',
                username: process.env.KEYCLOAK_USER,
                password: process.env.KEYCLOAK_PASSWORD,
            },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(error).toBeNull();
            expect(body).toBeDefined();
            token = body;
            done();
        });
    });
    // it('list users return 1 user (keycloak)', done => {
    //     const options = {
    //         method: 'GET',
    //         url: KEYCLOAK_URL + '/auth/admin/realms/master/users',
    //         headers: {
    //             authorization: token.token_type + ' ' + token.access_token
    //         },
    //         json: true,
    //     };
    //     request(options, (error, response, body) => {
    //         // expect(body.length).toEqual(1);
    //         expect(body[0].username).toEqual(process.env.KEYCLOAK_USER);
    //         done();
    //     })
    // });
    it('create user with details info', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + '/auth/admin/realms/master/users',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {
                email: 'user@gmail.com',
                username: 'user',
                attributes: {
                    abc: 123,
                    phone_number: '091234567'
                }
            },
            json: true,
        };
        request(options, (error, response, body) => {
            // expect(body.length).toEqual(1);
            console.log(response.statusCode);
            expect(response.statusCode).not.toEqual(400);
            done();
        })
    });
    it('list users return 2 users', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/admin/realms/master/users',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            // console.log(body);
            expect(body.length).not.toEqual(1);
            // expect(body[0].username).toEqual(process.env.KEYCLOAK_USER);
            expect(body[1].attributes.abc).toEqual(["123"]);
            // expect(body[1].attributes.phone_number).toEqual(["091234567"]);
            done();
        })
    });
    // it('find user by username then delete', done => {
    //     const options = {
    //         method: 'GET',
    //         url: KEYCLOAK_URL + '/auth/admin/realms/master/users?search=user&briefRepresentation=false',
    //         headers: { authorization: token.token_type + ' ' + token.access_token },
    //         json: true,
    //     };
    //     request(options, (error, response, body) => {
    //         expect(response.statusCode).not.toEqual(400);
    //         // expect(body[0].attributes).toBeDefined();
    //         for (let i = 0; i < body.length; i++) {
    //             const user = body[i];
    //             const options = {
    //                 method: 'DELETE',
    //                 url: KEYCLOAK_URL + '/auth/admin/realms/master/users/' + user.id,
    //                 headers: { authorization: token.token_type + ' ' + token.access_token },
    //                 json: true,
    //             };
    //             request(options, (error, response, body) => {
    //                 expect(response.statusCode).toEqual(204);
    //                 done();
    //             })
    //         }
    //         // done();
    //     })
    // });
});
describe('Test realm', () => {
    it('Create realm', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + '/auth/admin/realms/',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {
                realm: 'test',
                enabled: true,
                accessTokenLifespan: 3600
            },
            json: true,
        };
        request(options, (error, response, body) => {
            // expect(body.length).toEqual(1);
            if (body) { console.log(body); }
            console.log(response.statusCode);
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });
    it('Get Authorization Endpoints', done => {
        request({
            url: KEYCLOAK_URL + '/auth/realms/test/.well-known/uma2-configuration',
            json: true
        }, (error, response, body) => {
            if (error) {
                console.log(error)
            } else {
                // console.log(response)
                // console.log(body)
            }
            expect(error).toBeNull();
            expect(body).toBeDefined();
            UMA2_CONFIG = body;
            done();
        })
    });
    it('Create my-app client', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/clients',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {
                clientId: 'my-app',
                serviceAccountsEnabled: true,
                authorizationServicesEnabled: true,
            },
            json: true,
        };
        request(options, (error, response, body) => {
            // expect(body.length).toEqual(1);
            console.log(response.statusCode);
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            console.log(body);
            // myAppClient = body;
            done();
        })
    });
    it('Get my-app client', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/clients/?clientId=my-app',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            // expect(body.length).toEqual(1);
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            myAppClient = body[0];
            done();
        })
    });
    it('Get client secret', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/clients/${myAppClient.id}/client-secret`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            console.log(body);
            myAppClient.secret = body;
            done();
        })
    });
    it ('Get PAT', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/realms/test/protocol/openid-connect/token`,
            form: {
                grant_type: 'client_credentials',
                client_id: myAppClient.clientId,
                client_secret: myAppClient.secret.value,
            },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            expect(error).toBeNull();
            expect(body).toBeDefined();
            // console.log(body);
            myAppClient.pat = body as Token;
            done();
        });
    });
});

describe('Realm Management Client', () => {
    it('Get client info', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/clients/?clientId=realm-management',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(body.length).toEqual(1);
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            realmManagementClient = body[0];
            done();
        })
    });
    it('POLICY: users are allow to manage users in owned groups', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/clients/${realmManagementClient.id}/authz/resource-server/policy/js`,
            headers: { authorization: token.token_type + ' ' + token.access_token},
            json: true,
            body: {
                name: 'policy-1',
                type: 'js',
                logic: 'POSITIVE',
                decisionStrategy: 'UNANIMOUS',
                code: `
                    
                `
            }
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            console.log(body);
            done();
        })
    })

});

describe('Create groups', () => {
    it('/product-a', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/groups',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {
                name: 'product-a',
                path: '/product-a',
                subGroups: [
                    { name: 'product admin', path: '/product-a/admin', },
                ]
            },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('get group /product-a', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/groups?name=product-a',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            productAGroup = body[0];
            done();
        })
    });


    it('/product-a/admin', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${productAGroup.id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: { name: 'product admin', path: '/product-a/admin', },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('/product-a/allow-create-project', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${productAGroup.id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: { name: 'allow-create-project' },
            json: true,
        };

        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('/product-a/project-1', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${productAGroup.id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {name: 'project-1', path: '/product-a/project-1'},
            json: true,
        };
        request(options, (error, response, body) => {
            if (body) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('/product-a/project-2', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${productAGroup.id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {name: 'project-2', path: '/product-a/project-2'},
            json: true,
        };
        request(options, (error, response, body) => {
            if (body) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('get group /product-a 2', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/groups?name=product-a',
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            productAGroup = body[0];
            done();
        })
    });

    it('/product-a/project-1/end-user', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${_.find(productAGroup.subGroups, p => p.name === 'project-1').id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {name: 'end-user', path: '/product-a/project-1/end-user'},
            json: true,
        };
        request(options, (error, response, body) => {
            if (body) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('/product-a/project-1/admin', done => {
        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/groups/${_.find(productAGroup.subGroups, p => p.name === 'project-1').id}/children`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            body: {name: 'project admin', path: '/product-a/project-1/admin'},
            json: true,
        };
        request(options, (error, response, body) => {
            if (body) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    });

    it('assign user to /product-a/allow-create-project', async done => {
        const user = await createUser() as User;
        const options = {
            method: 'PUT',
            url: KEYCLOAK_URL + `/auth/admin/realms/test/users/${user.id}/groups/${_.find(productAGroup.subGroups, p => p.path === '/product-a/allow-create-project').id}`,
            headers: { authorization: token.token_type + ' ' + token.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            if (body) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            users.allowCreateProject = user;
            done();
        });
    });

    it('create new group using guest account should fail', async done => {
        const user = await createUser() as User;
        const token = await issueNewToken(user);

        const options = {
            method: 'POST',
            url: KEYCLOAK_URL + '/auth/admin/realms/test/groups',
            headers: { authorization: 'bearer ' + token.access_token },
            body: { name: 'group-that-should-not-exists' },
            json: true,
        };
        request(options, (error, response, body) => {
            expect(response.statusCode).toEqual(401);
            done();
        })
    })
});

describe('Managing Resources', () => {
    it('List resource set of my-app', done => {
        const options = {
            method: 'GET',
            url: KEYCLOAK_URL + '/auth/realms/test/authz/protection/resource_set',
            headers: { authorization: myAppClient.pat.token_type + ' ' + myAppClient.pat.access_token },
            json: true,
        };
        request(options, (error, response, body) => {
            // expect(body.length).toEqual(1);
            if (body && body.error) { console.log(body); }
            expect(response.statusCode).not.toEqual(400);
            expect(response.statusCode).not.toEqual(405);
            done();
        })
    })
});

// describe('User-Managed Access', () => {
//    it('')
// });

// describe('Managing Resource Permissions using the Policy API', () => {
//     it('List policies', done => {
//         const options = {
//             method: 'GET',
//             url: KEYCLOAK_URL + `/auth/realms/test/clients/${realmManagementClient.id}/authz/resource-server/policy`,
//             headers: { authorization: myAppClient.pat.token_type + ' ' + myAppClient.pat.access_token },
//             json: true,
//         };
//         request(options, (error, response, body) => {
//             // expect(body.length).toEqual(1);
//             if (body && body.error) { console.log(body); }
//             expect(response.statusCode).not.toEqual(400);
//             expect(response.statusCode).not.toEqual(405);
//             done();
//         })
//     })
// });


// describe('Cleanup tests', () => {
//     it('delete all users', done => {
//         const options = {
//             method: 'GET',
//             url: KEYCLOAK_URL + '/auth/admin/realms/master/users?search=&briefRepresentation=true',
//             headers: { authorization: token.token_type + ' ' + token.access_token },
//             json: true,
//         };
//         request(options, (error, response, body) => {
//             expect(response.statusCode).not.toEqual(400);
//             const promises = _.each(body, user => {
//                 if (user.username !== process.env.KEYCLOAK_USER) {
//                     return new Promise((resolve => {
//                         const options = {
//                             method: 'DELETE',
//                             url: KEYCLOAK_URL + '/auth/admin/realms/master/users/' + user.id,
//                             headers: {authorization: token.token_type + ' ' + token.access_token},
//                             json: true,
//                         };
//                         request(options, (error, response, body) => {
//                             expect(response.statusCode).toEqual(204);
//                             resolve();
//                         })
//                     }))
//                 }
//             });
//             Promise.all(promises).then(results => {
//                 done();
//             })
//         })
//     });
// });

function randomString() { return _.times(16, () => _.random(35).toString(36)).join(''); }

function createUser() {
    const options = {
        method: 'POST',
        url: KEYCLOAK_URL + '/auth/admin/realms/test/users',
        headers: {authorization: token.token_type + ' ' + token.access_token},
        body: {
            email: randomString() + '@gmail.com',
            username: randomString(),
            credentials: [
                {
                    type: 'password',
                    value: DEFAULT_USER_PASSWORD
                }
            ],
            attributes: {
                abc: 123,
            }
        },
        json: true,
    };
    return new Promise(resolve => {
        request(options, (error, response, body) => {
            expect(response.statusCode).toEqual(201);
            getUserByUsername(options.body.username, (error, response, body) => {
                resolve(_.find(body, user => user.username === options.body.username))
            });

        })
    })
}

function getUserByUsername(username: string, callback) {
    const options = {
        method: 'GET',
        url: KEYCLOAK_URL + '/auth/admin/realms/test/users?username=' + username,
        headers: {authorization: token.token_type + ' ' + token.access_token},
        json: true,
    };
    request(options, callback);
}

function issueNewToken(user: User): Promise<Token> {
    const options = {
        method: 'POST',
        url: OPENID_CONFIG.token_endpoint,
        form: {
            grant_type: 'password',
            client_id: 'admin-cli',
            username: user.username,
            password: DEFAULT_USER_PASSWORD,
        },
        json: true,
    };
    return new Promise(resolve => {
        request(options, (error, response, body) => {
            expect(error).toBeNull();
            expect(body).toBeDefined();
            resolve(body as Token);
        });
    });
}
